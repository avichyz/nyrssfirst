import React, { Component } from 'react';
import { Segment, Button, Menu, Icon, Sidebar } from 'semantic-ui-react'
import Clock from 'react-live-clock';
import { Route, Link } from 'react-router-dom'
import UsersContainer from '../modules/users/UsersContainer';
import GalleryContainer from '../modules/gallery/GalleryContainer';
import MainContainer from '../modules/main/MainContainer';
import styles from './app.scss';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sidebarOpen: false,
      isSaturday: true,
      headerTitle: ""
    }

    this.handleSidebarOpen = this.handleSidebarOpen.bind(this);
    this.handleSidebarClose = this.handleSidebarClose.bind(this);
    this.toggleVisibility = this.toggleVisibility.bind(this);
    this.setSaturday = this.setSaturday.bind(this);
    this.setHeaderTitle = this.setHeaderTitle.bind(this);

    // Refresh once in 5 hours
    // setInterval(() => { window.location.reload(); }, 18000000);
  }

  handleSidebarOpen() {
    this.setState({ sidebarOpen: true });
  }
  handleSidebarClose() {
    this.setState({ sidebarOpen: false });
  }
  toggleVisibility() {
    this.state.sidebarOpen ? this.handleSidebarClose() : this.handleSidebarOpen();
  }
  setSaturday(isSaturday) {
    this.setState({isSaturday: isSaturday});
  }
  setHeaderTitle(title) {
    this.setState({headerTitle: title});
  }
  render() {
    const { sidebarOpen, isSaturday, headerTitle } = this.state
    
    return (
      <div className={styles.app}>
        {
          isSaturday &&
          <div className={styles.saturdayBorder}/>
        }
        <header className={styles.appHeader}>
          <div className={styles.timeContainer}>
            <Clock format={'DD/MM/YYYY'} ticking={true}/>
            <Clock format={'HH:mm:ss'} ticking={true}/>
          </div>
          {
             isSaturday &&
            <h1 className={styles.appTitle}>שבת שלום</h1> ||
            <h1 className={styles.appTitle}>{headerTitle}</h1>

          }
          <h1 className={styles.appTitle}>נחל ירקון 28</h1>
        </header>
        {
          isSaturday &&
          <div className={styles.saturdayBorder}/>
        }
        <Sidebar.Pushable as={Segment} className={styles.sideBar}>
          <Sidebar
            as={Menu}
            animation='overlay'
            width='thin'
            direction='right'
            visible={sidebarOpen}
            icon='labeled'
            vertical
            inverted>
            <Link to="/main">
              <Menu.Item name='main'>
                <Icon name='home' />
                Home
            </Menu.Item>
            </Link>
          </Sidebar>
          <Sidebar.Pusher 
          className={styles.content}
          onClick={this.handleSidebarClose}>
            <Segment basic className={styles.basicSegment}>
              <Route exact path="/" 
                render={(props) => <MainContainer 
                  isSaturday={isSaturday} 
                  setSaturday={this.setSaturday}
                  setHeaderTitle={this.setHeaderTitle}/>}
               />
              <Route exact path="/gallery" component={GalleryContainer} />
              <Route exact path="/users" component={UsersContainer} />
            </Segment>
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </div>
    )
  }
}

export default App;
