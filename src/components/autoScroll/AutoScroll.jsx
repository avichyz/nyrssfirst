import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Segment } from 'semantic-ui-react';
import jQuery from 'jquery';
import classNames from 'classnames';
import styles from './autoScroll.scss';

const $ = window.$;

const propTypes = {
    className: PropTypes.string,
    id: PropTypes.string,
    speedDuplicator: PropTypes.number,
    callbackOnCompletionOfScroll: PropTypes.func
}
class AutoScroll extends Component {
    componentDidMount() {
        if(this.props && this.props.id) {
            try {
                let content_scroll = jQuery(`.marquee.${this.props.id}`);
                let height = content_scroll.height();
                let keyframeName = `marquee_js_generated_keyframe_${this.props.id}`
                let newKeyFrame = {
                    name: keyframeName,
                    '0%': { 'top': '7em' },
                    '100%': { 'top': `-${height}px` }
                }
                
                let speedDuplicator = this.props.speedDuplicator || 1;
                let seconds = (height / 50) * (1/speedDuplicator);

                var supportedFlag = $.keyframe.isSupported;
                console.log(newKeyFrame);
                console.log(`Height:${height}`);
                console.log(`Seconds:${seconds}`);
                $.keyframe.define([newKeyFrame]);

                $(content_scroll).playKeyframe(
                    `${keyframeName} ${seconds}s linear infinite`, ()=>{
                        this.props.callbackOnCompletionOfScroll && 
                        this.props.callbackOnCompletionOfScroll()
                    }
                );

                $(content_scroll).hover(
                    function () { $(content_scroll).pauseKeyframe(); },
                    function () { $(content_scroll).resumeKeyframe(); });
            }
            catch (e){
                console.log(e);
            }
        }
    }
    render() {
        const { className, id } = this.props;
        const segmentClasses = classNames(styles.container, className)
        const contentClassName = `marquee ${id}`;
        return (
            <Segment className={segmentClasses}>
                <div className="microsoft container">
                    <div className={contentClassName}>
                        {this.props.children}
                    </div>
                </div>
            </Segment>
        )
    }
}

AutoScroll.propTypes = propTypes;
export default AutoScroll;
