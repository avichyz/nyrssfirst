var hebrew;
var storageParasha = window.localStorage;
var viewportwidth;
var viewportheight;;
var month;
var year;
var day;
var hYearNow;
var hMonthNow;
var placesDropDownList;
var methodDropDownList;
var countriesDropDownList ;
var isLeap = false ;
var cxml = new CrossBrowserXml();
var numAnonimousComments;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
var commentsString = "";
var commentsStringDisplayed = "";
var holidayCommentsStringDisplayed = "";
var notHide = true;
var monthPattern = /^d\d{8}$/;
var holidayPattern = /^\d{8}.+$/;
var level = 0;

function GotoDate() {
    var m = document.getElementById("monthSelect");
    var month = m ? m.value : "1";
    var year = document.getElementById("yearTextBox").value;
    hebrew = false;
    GoTo(year, month);
}

function GotoHDate() {
    var h = document.getElementById("hMonth");
    var month = h ? h.value : "1";
    var year = document.getElementById("hYear").value;
    try { year = decodeYear(year); } catch (ccc) { }
    hebrew = true;
    GoTo(year, month);
}

function SchoolYear() {
    if (localStorage["SchoolYear"] == null || localStorage["SchoolYear"] == "n") {
        localStorage["SchoolYear"] = "y";
    } else {
        localStorage["SchoolYear"] = "n";
    }
    location.href = "annual-calendar.aspx?" + ((localStorage["SchoolYear"] === "y")?"m1=9&":"") +( hebrew?"h":"") + "year=" + year + "&hebrew=" + hebrew.toString();
}

function openMobile(td)
{
    //alert('openMobile');
    while (td.tagName != 'TD') td = td.parentNode;
    //showHideMobileDayPanel(true);
    location.href = 'MobileDay.aspx?date=' + td.date + "&mobilePrevPage=" + location.href + "&hebrew=" + hebrew.toString();
    //__doPostBack('content_refresh', td.date);
}

function initVariables(heb, hmonth, gmonth, hyear, gyear, pdl, mdl, cdl, countryId, placeId, methodId, currentHYear, currentHMonth, leap, userFontSize) 
{
    try 
    {
        //loadAll();
        //SynchAll();
        hebrew = heb;
        month = hebrew ? hmonth : gmonth;
        year = hebrew ? hyear : gyear;
        placesDropDownList = pdl;
        methodDropDownList = mdl;
        countriesDropDownList = cdl ;
        try { placesDropDownList.value=placeId; } catch (p) { }
        try { countriesDropDownList.value = countryId; } catch (c) { }
        try{ methodDropDownList.options[methodId].selected = true;} catch(m){}
        hYearNow = currentHYear;
        hMonthNow = currentHMonth;
        isLeap = leap;
        if (!isNaN(Number(userFontSize))) level = Number(userFontSize);
        console.log("userFontSize=" + userFontSize + " level=" + level);
        foo();
        setFontSize();
    }
    catch (xx)
    {
        Console.write(">>>" + xx);
        //alert(">>>" + xx);
    }
}


function ShowAll()
{
    var retval = '';
    for (var year = 2015; year <= Date.now().getFullYear(); year++)
        for (var month = 1; month < 13; month++)
            for (var day = 1; day < 32; day++) {
                var sm = month.toString(); if (sm.length < 2) sm = "0" + sm;
                var sd = day.toString(); if (sd.length < 2) sd = "0" + sd;
                var key = 'd' + year + sm + sd;
                var val = localStorage.getItem(key);
                if (val) retval += (key +"=" + val + "&");
            }
    return retval.replace(/^\"+|\"+$/g, '').replace(/^\&+|\&+$/g, '');
}


function loadAll()
{
    commentsString = '';
    try {
        var calendar = document.getElementById("calendar");
        if (!calendar) return;
        var table = calendar.getElementsByTagName('table')[0];
        for (var i = 1; i < table.rows.length; i++)
            for (j = 1; j < table.rows[i].cells.length; j++)
                loadContent(table.rows[i].cells[j]);
        //        synchComments();

    } catch (xxx) { alert(xxx); }
}

function loadContent(o)
{
    try
    {
        var td = o;
        var divs = o.getElementsByTagName('DIV');
        if (!divs || !divs.length) return;
        o = divs[divs.length - 1];
        var parts = o.id.split('_');
        var sid = parts[parts.length - 1];
        td.date = sid.substr(1);
        var eTxt = null;
        var lTxt = null;
        loadContentDiv(o);
    }
    catch (xxx) { }
}

function loadContentDiv(o)
{    
    try
    {
        var eTxt = null;
        var lTxt = null;
        var id = o.id;

        //handle anonimous comment on month page. id=content_monthlyCalendar_d20171003
        if (id.startsWith("content_monthlyCalendar_"))
        {
            id = id.substr("content_monthlyCalendar_".length);
            //if (id.length > 9) id = id.substr(id.length - 9, id.length);
            //console.log("loadContentDiv: " + id);
            //localStorage.setItem(id, 'aaaa');
            //localStorage.removeItem(id);
            //console.log(localStorage.getItem(id));
            if (o.style.behavior != undefined)
            { // Explorer userdata
                o.load(id);
                eTxt = o.getAttribute("sPersistText");
            }
            if (localStorage && localStorage.getItem != undefined)
            { // HTML 5 localstorageParasha
                lTxt = localStorage.getItem(id);
            }
        }
        //handle anonimous comment on holidays page
        else
        {
            if (id.length > 9) id = id.substr(id.length - 9, id.length);
            if (o.style.behavior != undefined) { // Explorer userdata
                o.load(id);
                eTxt = o.getAttribute("sPersistText");
                if (etxt !== null) {
                    try {
                        o.removeAttribute("sPersistText");
                        o.remove(id);
                        o.setAttribute("sPersistText", eTxt);
                        o.save(o.id);
                    }
                    catch (xx) { }
                }
            }
            if (localStorage && localStorage.getItem != undefined) { // HTML 5 localstorageParasha
                lTxt = localStorage.getItem(id);
                if (lTxt !== null) {
                    localStorage.setItem(o.id, lTxt);
                    localStorage.removeItem(id);
                }
                lTxt = localStorage.getItem(o.id);
            }
        }

        var txt = eTxt ? eTxt : '' + lTxt ? lTxt : '';
        console.log("loadContentDiv: " + txt);
        if (eTxt != null || lTxt != null)
        {
            if (document.getElementById("logedInPanel"))
            {
                commentsString += o.id + "ZZZ" + txt + "VVV";
            }
            else o.innerHTML = txt;            
        }
        o.defaultValue = o.innerHTML;
    }
    catch (xxx)
    {
        alert(xxx);
    }
}

function stripHTML(ss)
{
    var withBR = ss.replace(/<br\s*[\/]?>/gi, "\n");//שישאר הBR
    var text = withBR.replace(/<\/?("[^"]*"|'[^']*'|[^>])*(>|$)/g, "");//שלא ישארו תגיות HTML
    return text;
}

function SaveContent(o)
{
    var myText= stripHTML(o.innerHTML);
    var picker = document.getElementById("colorPicker");
    if (picker != null && picker.title != "XXX") picker.style.display = 'none';
    if (!o.innerHTML && !o.defaultValue) return;
    if (o.innerHTML == o.defaultValue) return;
    try {
        var id = o.id;
        //if (id.length > 9) id = id.substr(id.length - 9, id.length);        
        if (id.startsWith("content_monthlyCalendar_"))
        {
            id = id.substr("content_monthlyCalendar_".length);
        }
        // If this is a registered user, save comment on server
        if (document.getElementById("logedInPanel"))
        {
            commentsString = o.id + "ZZZ" + o.innerHTML + "VVV";
            synchComments();
        }
        else //shorten anonimous comment on month page only (not on holidays page)
        {
            console.log(id + "=" + o.innerHTML);
            if (localStorage && localStorage.setItem)
                localStorage.setItem(id, o.innerHTML);
            else {
                if (o.innerText) // explorer
                {
                    try {
                        o.setAttribute("sPersistText", o.innerText);
                        o.save(id);
                    }
                    catch (xx) { }
                }
            }
        }
    }
    catch (x) {
    }
}


function CrossBrowserXml(doc) 
{
    this.xmlDoc = doc;
    this.setHandler = function (func) { this.onload = func; }

    var isExplorer = false ;
    try
    {
        var test = new ActiveXObject("Msxml2.DOMDocument") ;
        isExplorer = true ;
    }
    catch(aaa)
    {
        isExplorer = false ;
    }

    if (!isExplorer)
    {
        this.status = "loading";
        if (typeof (doc) == 'string')
        {
            var parser = new DOMParser();
            this.xmlDoc = parser.parseFromString(doc, "text/xml");
            this.status = this.xmlDoc.documentElement ? "ok" : "error";
        }
        if (!doc) this.xmlDoc = window.document.implementation.createDocument("", "", null);
        var frag;
        this.transform = function (node, img)
        {
            frag = xsltProcessor.transformToFragment(node, window.htmlDoc);
            //alert(new XMLSerializer().serializeToString(frag));
            img.parentNode.insertBefore(frag, img);
            // Add the root path to the source of images
            var images = img.parentNode.getElementsByTagName("img");
            for (var i = 0; i < images.length; i++) {
                var src = images[i].getAttribute("src");
                if (src[0] == 't') images[i].src = rootPath + "/" + src;
            }
        }
        this.findNode = function (xpath) { return this.xmlDoc.evaluate(xpath, this.xmlDoc.documentElement, null, XPathResult.ANY_TYPE, null).iterateNext() }
        //this.findNode = function (xpath) { return this.xmlDoc.evaluate(xpath, this.xmlDoc.documentElement, null, 0, null).iterateNext() }
        this.getStatus = function () { return this.status; }
        this.setHandler = function (func) { this.onload = func; }
        this.setProperty = function (n, v) { }
        this.load = function (url, tree)
        {
            var xmlReq = new window.XMLHttpRequest();
            xmlReq.open("GET", url, false);
            xmlReq.send(null);
            this.xmlDoc = xmlReq.responseXML;
            this.owner = tree;
            this.xmlDoc.owner = this;
            this.xmlDoc.loaded = this.loaded;
            this.xmlDoc.loaded();
        }
        this.loaded = function () {
            this.owner.status = this.owner.xmlDoc.documentElement ? "ok" : "error";
            this.owner.onload(this.owner.owner);
        }
        this.getProperty = function (x) { return "no"; }
    }
    else
    {
        if (!doc || typeof (doc) == 'string') this.xmlDoc = new ActiveXObject("Msxml2.DOMDocument");

        if (typeof (doc) == 'string') {
            this.xmlDoc.async = "false";
            this.xmlDoc.loadXML(doc);
        }
        this.transform = function (node, img) {
            var strHTML = node.transformNode(cbXslTree.xmlDoc);
            if (window.ampFilter)
                strHTML = strHTML.replace(window.ampFilter, '&');
            strHTML = strHTML.replace(/"tree\//g, '"' + rootPath + "/" + 'tree/');
            try { img.insertAdjacentHTML('beforeBegin', strHTML); } catch (xxx) { this.div.innerText = strHTML; }
        }
        this.findNode = function (xpath) { return this.xmlDoc.documentElement.selectSingleNode(xpath); }
        this.getStatus = function () {
            if (this.xmlDoc.readyState < 4) return 'loading';
            if (this.xmlDoc.parseError.errorCode == 0) return 'ok';
            if (this.xmlDoc.parseError.errorCode == -1072897514) return 'loading';
            this.errorMessage = this.xmlDoc.parseError.reason + ' (' + this.xmlDoc.parseError.errorCode + ') ' + this.xmlDoc.xml;
            return 'error';
        }
        this.badProperty = '';
        this.setProperty = function (n, v) { try { this.xmlDoc.setProperty(n, v); } catch (xxx) { this.badProperty = n; } }
        this.getProperty = function (n) { try { return this.xmlDoc.getProperty(n); } catch (xxx) { return true; } }
        this.load = function (url, tree) {
            this.owner = tree;
            if (!window.arrTrees) window.arrTrees = new Array();
            window.arrTrees.push(this);
            this.xmlDoc.onreadystatechange = this.readyStateChanged;
            this.xmlDoc.load(url);
        }
        this.readyStateChanged = function () {
            var remove = true;
            for (i = 0; i < window.arrTrees.length; i++) {
                if (window.arrTrees[i]) {
                    if (window.arrTrees[i].xmlDoc.readyState == 4) {
                        window.arrTrees[i].onload(window.arrTrees[i].owner);
                        window.arrTrees[i] = null;
                    }
                    else {
                        remove = false;
                    }
                }
            }
            if (remove) window.arrTrees = null;
        }
    }
}

function countryChanged() 
{
    cxml.setHandler(setPlaces);
    cxml.load("GetPlaces.aspx?country=" + countriesDropDownList.value, this);
}

function setPlaces() 
{
    placesDropDownList.options.length = 0;
    var nodes = cxml.xmlDoc.getElementsByTagName("place");
//    alert(cxml.xmlDoc);
//    alert(nodes.length);
//    alert(cxml.xmlDoc.documentElement.children);
    for (i = 0; i < Number(nodes.length); i++) 
    {
        var o = document.createElement("option");
        o.text = nodes[i].firstChild.nodeValue;
        o.value = nodes[i].getAttribute("ID");
        placesDropDownList.add(o);
    }
    var def = document.createElement("option");
    def.text = "נא לבחור עיר";
    def.selected = true;
    placesDropDownList.add(def);
}

function setValues() 
{
    var root = xml.findNode("/times");
    var nodes = root.childNodes;
    var types = root.getAttribute("daytype");
    var timeTable = document.getElementById("timeTable");
    for (i = 0; i < timeTable.rows.length; i++)
    {
        var type = timeTable.rows[i].getAttribute("showon");
        if (type) timeTable.rows[i].style.display = types.indexOf("," + type + ",") >= 0 ? '' : 'none';
    }
    for (i = 0; i < Number(nodes.length); i++) 
    {
        var x = document.getElementById(nodes[i].nodeName);
        if (x) 
        {
            try 
            {
                if (nodes[i].firstChild) x.innerHTML = nodes[i].firstChild.nodeValue;
                else x.innerHTML = "";
            }
            catch (xx) { alert(xx + "\r\n" + nodes[i].nodeName); }
        }
    }
}

var xml = new CrossBrowserXml();

function getDayValues(date, placeDropDown) 
{
    xml.setHandler(setValues);
    xml.load("GetTimes.aspx?date=" + date + "&placeId=" + GetSelectedIndex(placeDropDown), this);
}

function setCookie(cname, cvalue, exdays) 
{
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) 
{
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function decodeYear(hyear) 
{
    if (!hyear || hyear.length < 2) return 0;

    var y;
    if (!isNaN(Number(hyear))) return y;

    y = 5000;

    if (hyear[1] == "'") {
        y = (hyear.charCodeAt(0) - 'א'.charCodeAt(0) + 1) * 1000;
        hyear = hyear.substring(2);
    }
    hyear = hyear.replace("\"", "");
    while (hyear.length > 0 && hyear[0] >= 'ק') {
        y += "צקרשת".indexOf(hyear[0]) * 100;
        hyear = hyear.length == 1 ? "" : hyear.substring(1);
    }
    hyear = hyear.replace('ך', 'כ');
    hyear = hyear.replace('ם', 'מ');
    hyear = hyear.replace('ן', 'נ');
    hyear = hyear.replace('ף', 'פ');
    hyear = hyear.replace('ץ', 'צ');

    while (hyear.length > 0 && hyear[0] >= 'י') {
        y += ("טיכלמנסעפצ".indexOf(hyear[0])) * 10;
        hyear = hyear.length == 1 ? "" : hyear.substring(1);
    }
    if (hyear.length == 1) y += " אבגדהוזחט".indexOf(hyear[0]);
    return y;
}

function GetSelectedIndex(sel) 
{
    try
    {
        var i;
        for (i = 0; i < sel.options.length; i++) if (sel.value == sel.options[i].value) return i;
    }
    catch(xx)
    {
        //alert(sel) ;
    }
    return -1;
}

function showWeekNumbers(show) 
{
    var tds = document.getElementsByTagName("TD");
    for (var i = 0; i < tds.length; i++) 
    {
        if (tds[i].className.indexOf('week') >= 0) tds[i].style.display = show ? '' : 'none';
    }
}

function GetDate(dt) 
{
    //location.href = 'TodayTimes.aspx?today=' + dt.date;
    var url = 'TodayTimes.aspx?today=' + dt.date;
    url = RabenuTamRestore(url);
    location.href = url;
}

function hideTimes(e) 
{
//    var div = document.getElementById("dayTimesDiv");
//    if (!div) return;
//    if (div.style) div.style.display = 'none';

    var o = e && e.target ? e.target : event.srcElement;
    if (o.tagName == "INPUT") return;

    //if (document.body.clientWidth < 400) 
    //{
    //    while (o.nodeName != 'TD' && o != null) o = o.parentNode;
    //    o = o.getElementsByTagName("img")[0];
    //    if (!o || o.className != 'timesIcon') return;
    //}

    if (o.nodeName == 'IMG' && o.parentNode.parentNode && o.parentNode.parentNode.date) 
    {
        if (document.body.clientWidth < 981) openMobile(o);
        //    location.href = 'MobileDay.aspx?date=' + o.parentNode.parentNode.date +
        //        "&mobilePrevPage=" + location.href + "&hebrew=" + hebrew.toString();
        //else
        GetDate(o.parentNode.parentNode);        
    }
    else 
    {
        var td = o.tagName == 'TD' ? o : o.parentNode;
        while (td && td.nodeName != 'TD') td = td.parentNode;
        if (td) 
        {
            if (document.body.clientWidth < 981) openMobile(td);
            //    location.href = 'MobileDay.aspx?date=' + td.date +
            //        "&mobilePrevPage=" + location.href + "&hebrew=" + hebrew.toString();
            var divs = td.getElementsByTagName('DIV');
            if (divs.length) divs[divs.length - 1].focus();
            var div = divs[divs.length - 1];
            //alert(div.contentEditable);
            preparePicker(div);
        }
    }
}

function getSelectionText() 
{
    var text = "";
    if (window.getSelection) 
    {
        text = window.getSelection().toString();
    }
    else if (document.selection && document.selection.type != "Control") 
    {
        text = document.selection.createRange().text;
    }
    return text;
}

function changeColor() 
{
    var selected = getSelectionText();
    var picker = document.getElementById("colorPicker");
    if (selected)
        editing.innerHTML = editing.innerHTML.replace(selected, selected.fontcolor(picker.value));
    else     
    {
        editing.style.color = picker.value;
        if (editing.innerHTML) editing.innerHTML = editing.innerHTML.fontcolor(picker.value);
    }
    picker.title = "בחר צבע";
    SaveContent(editing);
}

var editing;

function preparePicker(e) 
{
    var picker = document.getElementById("colorPicker");
    if (!picker) return;
    if (picker.type != 'color') return;

    editing = e;
    var left = 0, top = 0;
    
    while (e) 
    {
        left += e.offsetLeft;
        top += e.offsetTop;
        e = e.offsetParent;
    }
    top -= 25;
    picker.style.display = '';
    picker.personal = e;
    picker.style.top = top + "px";
    picker.style.left = left + "px";
}

function getPosition(e)
{
    var left = 0, top = 0;
    while (e) 
    {
        left += e.offsetLeft;
        top += e.offsetTop;
        e = e.offsetParent;
    }
    var div = document.getElementById("dayTimesDiv");

    div.style.display = '';

    if (document.body.clientWidth >= 900) 
    {
        div.style.left = left + "px";
        div.style.top = top + "px";
        div.style.width = "202px";
        div.style.height = "520px";
        div.style.top = (top - 130) + "px";
        div.style.left = (left - 25) + "px";
    }
    else 
    {
        var doc = document.documentElement;
        left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
        top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

        div.style.left = "0px" ;
        div.style.top = top + "px" ;
        div.style.width = "100%";
        div.style.height = "50em";        
    }

    div.style.opacity = 1;
}

function printDiv() 
{
    var divToPrint = document.getElementById("mainContentTop");
    var newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
}

function GoTo(year, month, selected) 
{
    var url = "";
    var place = placesDropDownList.value;
    var method = methodDropDownList.selectedIndex;
    var country = countriesDropDownList.value;
    if (hebrew)
        url = location.pathname + "?hmonth=" + month + "&hyear=" + escape(year) + "&placeId=" + place + "&methodId=" + method;
    else
        url = location.pathname + "?month=" + month + "&year=" + year + "&placeId=" + place + "&methodId=" + method;

//    if (country != 'ישראל') url += "&country=" + country;

    if (selected) url += "&selected=" + selected;
    url = RabenuTamRestore(url);
    location = url;
}

function changeMethod() 
{
    var method = methodDropDownList.selectedIndex;
    var url = location.toString();
    var existing = url.indexOf("methodId=");
    if (existing > 0) url = url.substring(0, existing-1);
    if (url.indexOf('?') < 0) url += "?methodId=" + method;
    else url += "&methodId=" + method;
    location = url;
}

function GoToDefault()
{
    GoTo(year, month);
}

function toggleShow(id, button) 
{
    var obj = document.getElementById(id);
    obj.style.display = obj.style.display == 'block' ? 'none' : 'block';
    //if (button) button.getElementsByTagName("img")[0].src = obj.style.display == 'block' ? "images/arrow-up.png" : "images/arrow-down.png";
    if (button) 
    {
        var u = button.src.toString();
        button.src = obj.style.display == 'none' ? u.replace('menu_active.png', 'menu.png') : u.replace('menu.png', 'menu_active.png');
    }
}

function Today() 
{
    if (hebrew) 
    {
        month = hMonthNow;
        year = hYearNow;
    }
    else
    {
        var date = new Date();
        month = date.getMonth()+1;
        year = date.getFullYear();
    }
    GoTo( year, month );
}

function navigate(deltaMonth, deltaYear) 
{
    first = hebrew ? 0 : 1 ; last = hebrew ? 11 : 12 ;
    month += deltaMonth;
    year += deltaYear;
    if ( hebrew && isLeap )
    {
        // 6 == Nissan, 12 == Adar A, 13 == Adar B
        if (month == 6 && deltaMonth == 1 || month == 5 && deltaMonth == -1) month = 13;
        if (month == 14) month = 6;
        if (month == 11 && deltaMonth == -1)
            month = 4;
        if (month == 12 && deltaMonth == 1) { month = 0; year++; }
        if (month == 5) month = 12;
        if (month > 11 && deltaYear != 0) month = 5;
    }
    else
        if (month > last) { month = first; year++; }
    if (month < first) { month = last; year--; }
    GoTo(year, month);
    return false;
}

function go() 
{
    var places = document.getElementById("selectPlaceDiv").getElementsByTagName("select")[0];
    getDayValues('', places);
    document.getElementById("zhayom").style.backgroundColor = "#0575F4";
    document.getElementById("zhayom").style.color = "#fff";
    document.getElementById("zshabat").style.backgroundColor = "#959290";
    document.getElementById("zshabat").style.color = "#fff";
}

function shabat() 
{
    var date = new Date;
    date.setDate(date.getDate() + (6 - date.getDay()));
    var d = String(date.getFullYear());
    if (date.getMonth() < 9) d += '0';
    d += String(date.getMonth() + 1);
    if (date.getDate() < 10) d += '0';
    d += date.getDate();
    var places = document.getElementById("selectPlaceDiv").getElementsByTagName("select")[0];
    getDayValues(d, places);
    document.getElementById("zhayom").style.backgroundColor = "#959290";
    document.getElementById("zhayom").style.color = "#000";
    document.getElementById("zshabat").style.backgroundColor = "#000";
    document.getElementById("zshabat").style.color = "#fff";
}

function foo() 
{
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequestHandler);
}

function endRequestHandler(sender, args) 
{
    loadAll();
}

function keyPress(e) 
{
    var x = e || window.event;
    var key = (x.keyCode || x.which);
    alert(key);
    if (key == 13 || key == 3)
    {
        //document.yourFormName.submit();
        alert("enter");
    }
}

function selectTab( n )
{
    var ul = document.getElementById("headerCenterMenu");
    var lis = ul.getElementsByTagName("li");
    var li = lis[n];
    li.innerHTML = li.innerHTML.toString().replace("<a", "<h1").replace("/a", "/h1");
}

function goMyProfile()
{
    location.href = 'Account/ProfilePage.aspx';
}

function showHideMobileDayPanel(show)
{
    document.getElementById("content_mobileDayPanel").style.display = show ? '' : 'none';
    //document.getElementById("content_Load").style.display = show ? '' : 'none';
    document.getElementById("overlay").style.display = show ? '' : 'none';
}

function afterLoad()
{
    loadAll();
    console.log("After Load level=" + level);
    setFontSize();
}


function minusFont()
{
    if (level > 0) level--;
    setFontSize();
    insertUserFontSize();
}

function plusFont()
{
    if ( level < 4 ) level++;
    setFontSize();
    insertUserFontSize();
}

function setFontSize()
{
    var elements = document.getElementsByClassName("th");
    var textElements = document.getElementsByClassName("changeFont");
    var fontSize = (level*2 + 12).toString() + "px" ;
    var fontSizeElement = (level*2 + 16).toString() + "px" ;

    for (var i = 0; i < elements.length; i++)       elements[i].style.fontSize = fontSizeElement;
    for (var i = 0; i < textElements.length; i++) textElements[i].style.fontSize = fontSize;    
}

function insertUserFontSize()
{
    console.log(location.pathname);
    var url = ( location.pathname.toLocaleLowerCase().indexOf("account") > 0 ? "../" : "" ) + "InsertUserFontSize.aspx?" + "&userFontSize=" + level;
    console.log(url);

    if (window.XMLHttpRequest) {
        http = new XMLHttpRequest();
        http.status = 600;
    }
    else {
        // code for older browsers
        http = new ActiveXObject("Microsoft.XMLHTTP");
    }
    http.onreadystatechange = function ()
    {
        //console.log(http.readyState);
        if (http.readyState == 4)
        {
            //console.log(http.status);
            try {
                if (http.status == 200)
                {
                    console.log("level: " + level + " saved");
                }
                else {
                    alert("חלה תקלה בשמירת גודל הפונט");
                }
            }
            catch (ppp) { alert(ppp); }
            window.startSynch = 0;
        }
    };

    http.open("GET", url, true);
    http.send();
}


function displayIcon(x)
{
    var first = x.childNodes[0];
    var rem = first.childNodes[1];
    var dat = first.childNodes[2];
    rem.className = "timesIcon addIcon  display";
    dat.className = "timesIcon addIcon  display";
}

function dispalyNoneIcon(x)
{
    var first = x.childNodes[0];
    var rem = first.childNodes[1];
    var dat = first.childNodes[2];
    rem.className = "timesIcon addIcon";
    dat.className = "timesIcon addIcon";
}


function changeClassOfDiv()
{
    var leftDiv = document.getElementsByClassName("leftColmun");
    var right = document.getElementById("rightDiv");
    var left = document.getElementById("leftDiv");

    if (left.className == "colmun leftColmun")
    {
        right.className = right.className + " leftColmun";
        left.className = "colmun";
        self.print();
        changeClassOfDiv();
    }
    else
        if (right.className == "colmun leftColmun")
        {
            left.className = left.className + " leftColmun";
            right.className = "colmun";           
        }
}