import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image as CImage, Video, Transformation, CloudinaryContext } from 'cloudinary-react';
import { Image, Segment, Menu, Button } from 'semantic-ui-react';
import styles from './gallery.scss';

export default class Gallery extends Component {
    state = { activeItem: 'שולחנות' }
    handleItemClick = (e, { name }) => {
        this.setState({ activeItem: name }, this.props.handleCategoryClick(name))
        }

    static propTypes = {
        selectedImageTitle: PropTypes.string,
        selectedImageSrc: PropTypes.string
    }
    render() {
        const { activeItem } = this.state
        const { selectedImageSrc } = this.props;
        return (
            <Segment className={styles.segment}>
                <div className={styles.contentContainer}>
                    <Menu vertical>
                        <Menu.Item name='שולחנות' 
                            active={activeItem === 'שולחנות'} 
                            onClick={this.handleItemClick} />
                        <Menu.Item
                            name='ארונות'
                            active={activeItem === 'ארונות'}
                            onClick={this.handleItemClick}
                        />
                        <Menu.Item
                            name='כיסאות'
                            active={activeItem === 'כיסאות'}
                            onClick={this.handleItemClick}
                        />
                        <Menu.Item
                            name='סלונים'
                            active={activeItem === 'סלונים'}
                            onClick={this.handleItemClick}
                        />
                    </Menu>
                    {
                        this.props.prevImage && 
                    <Button onClick={this.props.prevImage}>{`<`}</Button>
                    }
                    <CImage cloudName="avichyz" width="100" crop="scale" publicId="mali/410406179.jpg"/>
                    {
                        this.props.nextImage && 
                        <Button onClick={this.props.nextImage}>{`>`}</Button>
                    }
                </div>
            </Segment>
        )
    }
}
