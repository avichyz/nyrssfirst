import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Gallery from './Gallery';

const imageUrls = [
    {
        type: 'שולחנות', 
        url:'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTEhIVFRUXFxUVGBgXFRgVGBoXFxcXFxcXFxcYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQFS0lHx0vLS0tLS0uLS0tLS0tLS0tKy0tLi0rKystLS0tLS0tLS0tKzUtLS0tLS0tLS0rLSsrLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAAcBAAAAAAAAAAAAAAAAAQMEBQYHCAL/xABEEAACAQICBQgGBwYFBQAAAAAAAQIDEQQhBQYSMXEHE0FRYYGRoSIyUrHB0RRCcoKSsuEkYnOiwvAVI0NTYwgWNETD/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECAwQFBv/EACkRAQACAgEDAwIHAQAAAAAAAAABAgMRMQQSIQVBURNhFCIyQmJxsVL/2gAMAwEAAhEDEQA/AN4gAAAAAAAAAAAAAAAAAAAAJOLxEacHOW5f2kYbjdL1Kju5Wj0RTy7+sn646SvNUk8o5y+09y7l7zHY1TyOt6iZt2VnxDbHX3XaljJLdJrg2iso6bqx+tfir/qWKNQ9qocNct68TLSaxLK6Gsftw74/J/MueG0pSnulZ9TyZgsahNjUOrH1+SvPlSccNhAwSljJx9WUlwbRWUtN1V9a/FJ/qddfUKTzCk45ZeCw4bWJbpxt2x+TLzh68Zrai00dePNTJ+mVJrMcpoANUAAAAAAAAAAAAAAAAAAAAAAU2kMWqVOVR7orxe5LxsVJh2u2kLyVGLyXpS4vcvDPvMc+X6dJstWNyxfEVnKTlLfJtvi2S9ohJnhs+fmdulNjImqoUbmRUyNC4QrE5VS2RmTYVAK+VUgq5SKR5k2iBPr4+yMp1BlKVKpJ7nOy7kr+9GAYqRtLVXC83haK6XFSfGXpfE9DoKfn38M8k+F2AB7DAAAAAAAAAAAAAAAAAAAAAASsTWUISm90U2+41djcQ5ylOW+TbfeZdrhpRKPMxd27OXYlmlx3GFzlc8fr80Wt2xPH+t8dfdJkyVKZUvDtkirganQr95522qTzhFTKd053zi01v/vqEKhMWFZGRMiyTSJ6RZCbGRGbyPCPNeXUVErD0OcqwgvrSjHxdjckI2SS3LI1lqRhtvFxfRBSl5WXm0zZx7PQU1SZ+WGSfIADvZgAAAAAAAAPM5pK7aSXS8i2V9YKEfrbX2Vfz3FL5KU/VOkxEyuoMdqa1w+rTk+LS+ZSVdbZdEILi2/kc9utwx+5bssy0GD1daKr+tFcIr4lFW0/Ue+pLxt7jK3qOOOIlP0pbDlJLe0uORT1NI0o76kfG/uNa1dJ3zbv2vMky0kzC3qc+1FoxfdsWrp+itzlLgvnYteP1lk01Tjs9rzfd0JmFvST6zzPHXOa/X5reN6/paMcQn4io23nd559b6zzCJIpO5PjM5dtE6MSfC5JhInRZAjVoKS9JX6v0fQUOJ0Unmnfjv7pLPxuXKMjzOYgWT6LKPrImpFxnmU1SnYtEoSUynrPtJs+wpKrZaORm/JvhvRq1Lb2oLuu370ZqWTU3C83hKfXK8397d5JF7Poenr244hzWncgANlQAAAAAKHSukoUIbUs28ox6W/l2la2ay03pJ1qkpvduiuqK3fPvOXqs/0q+OZWpXcpulNLzqO85cIr1VwRaKmMKPEVCklUPCtabTuZ8uiIXCWMZLeKKBzPHOldpV8sT2nh1yi5wc6V2lVuqedsp+cG0QlUc4RVQp9oKWZAu8HZLgTYTKVyPakVixpWRmToTKOMidCReJQq9uxBzKdSuTYUpPdF+BYTE7nuT7Lkyhhn05EK06a3vPsbv5BC31qS6PBlFCjt1IwWblJRXe7HvGYh3snl25vxRd9RMFzmJU3uppyfHcvN37jpwY5taI+VbS2Th6ShGMVuilFcErEwA+hcwAAAAAAAC3aw19jD1Gt+zsrjLL4muHQct2XaZ1rpK2G4ziveYZSZ4/qMzN4j7NsfCiqaJv8AX8iQ9Bfv+X6l7iLHm+WqwS0A/wDc/l/UkvV+f+5HwZkqRBxBtjD1eqe3HzIf9v1fbj5/IyhEbEJ2xb/Aq3tQ8X8h/glb93x/QynZIxgPJtii0NW6o+J7WhavXHx/QyepKEc5Mpp6TgvVjf8AvrY7Jk7llWGq3tzb7Gs0V1HRVV70o8WibU0xLosl4/IpJ6Qk/rv3e4RgO5co6OjHOc/D5sjz1CO5OXi/0LJPE/3v8zxzrNYxRCu1+/xVL1YJLt+SJFTS0uvwXxdyz7RBsv2QjauqaQb63xd/Ip6uJbJNjyok9sD2nc2TqBgtig5tZzl5RyXm2a5wtPaaXW0jcujcNzdKFP2Ypd9s/M7uipu/d8M8k+FSAD1GQAAAAAAACxa6wvhZPqlF+dviYNQmbK0vhedo1Ie1F24rNeaRqylOx5XqFfzRLbHPhdIsmFJSqlTFnltEbESNyKA8NCx7KXGV9nLxJ0PdSqlvZacbph7oeJRY7FOTtfIpka1ohMnVbebbPO0QRFRLaC5FBEQB6SIEbEgkD1YgQFgEekBe9TcFzmIhllF7T+7n77G1DDeTvCWjOq1vtFe9/AzI9fo6duPfyxvPkAB1KAAAAAAAABrXWzR/NV5WXoz9Nd+9eN/I2UWzT+iliKTj9ZZxfb1cGYdRi+pTXvC1Z1LWMa1iop4opa9CUW1JNNOzT6GSr2PDtRvtd44kmqsWRVWR599ZTtSvfPFg0jXu32k5Vn0lvxLVyawSlXI2IJHpGqpY9WI2IsJQI3IEYgRREESBAXPTIMBE9wRCKK/QuD52tTh7Ulfgs35JkxG51CGy9WsJzeGpxtm1tPjLP3WLoQSsRPerXtiI+HPIACwAAAAAAAABglYquoQlOW6MXJ8Erv3AaM101snDSWIy26UZqns9PoRSk4vr2rlXgcdSrpypS2rWutzi3uunu3eRrnSGIlUnKcvWm5TfGTbfmzdPIhodQwVStOKbr1Ha6306a2Vf73OHFfBXJPw2me2Fh2CXI2ji9VsNNuWw4N+w7Lw3Fl0nqTaEnQntStdRnZXfVtLd4HNboskceSLwwe5TVY5lJjtMujUlTq4ecJxycZSV15bu1Flqa7U1KSdGas7ZSi7242M/w2WP2rd0MkjE97JhmL16d7UqKt++8/CPzKCet2Lk/WhHsUFbzbLx0mSUbbD2T1smtausmLl/rNfZUV5pXKV6YxD/APYq/jl8y0dHb5O5tVRINJb2lxdjUdTFVZb6tR8ZyfxPDTfrO/F395eOi/kdzbUsbSW+rTX34/Mp6mncLHfiKf4k/catnBdSJagWjoq/9I7mzqmteDX+tf7MJv4EuOt+FbSUpZtLOEks3vbe5GuIxJiiW/B4/unboNanYppOKpNPp5zKz6V6OZkmqurTw7dSo4udrJLNK+93fSzH+RTWb6Tg/o9R3q4a0c97pP1H22s49y6zYxtTpsdZi0Qym08AAOhUAAAAAAAAAAAxflMx3NaNxDTs5xVJffai/JyMoNYcuOOtRoUU/WnKo12QWyvOb8CLT4TXlped5O0Vdt2S628kvE6n1e0asNhqNBbqdOMOLS9J97u+80FyZaI+kaSopq8abdaXTlBej/O4HRpTHHuveQAGjNYdatVKGOhaqtma9SpFLbj2dsex+RytpTD7NapHfapUjfr2ZNX8jsc5A0vnWqPrqVH4zZSy9Fs5q8lZX7N5VUNEYifqYevP7NGpL3RMo5KaG1pfBrqnUl+GjUl8DqAmI2mZ05MwmpmkZ+rgcQ+NKUPz2K+jyZ6WluwM++pRj+aojqQE6V7pc14fkf0rLfSpQ+3Wj/RtGPaz6tV8BWVDEKO04qacW5RabaybS6U+g61MH5T9RXpOnSdKcKdanLKUk7OEvWi7Z77Nd/WRMEWaq1J5MnpGhz8cZCmlOUJR5lzkmrPN7cd6afeZbQ5CaX18bUf2acI+9syrkz1JqaMhVjUxCq844vZjBxjFxTTabbbbuurcjNiYgmWrqPIfgl62IxMuEqcf/mad1p0BPBYmph574PJ+1B+pNcV8UdZmu+WPVT6VhvpNKN61BNtLfOle8lxjnJfe6yJgizTmomn3gcZSr3exfYqLrpyyl4ZS+6dSUqiklKLumk01uaeaZx8kb+5FtZOfwrw03/mYeyj20n6v4XePDZIrK1o92xgAXZgAAAAAAAAAAGiOWPH85j3BPKlThDvd5v8AOvA3ucu606R57FVq17qdSbXatq0fJIpknwvTls7kL0XanXxLWcpKjF9kUpStxckvum1Cxaj6K+jYHD0mrSUFKf25+lLzdu4vpasahW07kABKA5Axmc5PrlN/zM6+k8jj2Urv++kzv7NKe7OORKjfSsH7NKtLyUf6zos0PyCUb46tL2aD/mnD5M3wWrwrbkABZUAAAAACDREAc38qOqn0LFN042oVbzp9Sz9On91tW7Guplr1K088Fi6VdX2U9mouunLKXhvXakdCa76uRx2FnRdlNenSl7NRbu55xfY2cyYnDypzlCcXGUW4yi96knZp8GZ28NazuNOuKFWM4xlFpxklJNbmmrpruPZrbkV1i57DPCzfp0LbHbSfq/hd1w2TZJeJ2zmNSAAlAAAAAAAAAWzD6u4SE+chhKEZ3vtRowUr8UrlzAAAAAABLru0ZPsfuOQI9HA68x7tSqP9yX5Wchw+CM78w0p7ttf9PeH/AMzGVP3aMV41G/cjdJqn/p/p/s+Kl/ywj4QT/qNrF68K25AASqAAAAAAAAGm+WrVTZksdSjlJqNZLoluhPvyi+1R6zchTaRwUK1KdKpHahOLjJdj+JExtMTpzJqjpyWCxVKur2i7TXtU5ZTXhmu1I6fw1eNSEZwalGSUotbnFq6a7mcx61aAng8TOhPOzvGXtQd9mfet/ambX5FdYedw8sJN+nRzhfppPo+68uDRWs6nS943G2ygAXZgAAAAAAAAAAAAAAAKTSztQqv/AI6n5WckJfA6004/2av/AAqn5GcnTRnfmGlOJb35B8Ps6PqS9vEzl3KnSj74s2QYVyPUdnRdH96VWXjUl8jNS8cKW5AASgAAAAAAAAAAFi1o1Tw2PiliIPajfZnB7M4p71fpXY0yXq5qXg8FJzoU3zjWy6k5OUrO11nkr2W5IyEEahO5AASgAAAAAAAAAAAAAAABb9Yn+y4j+DV/IzlSqdT60P8AY8R/Bq/kZyxiacpS2Ypyk9ySu3wS3md+Ya4+JdLcm1LZ0XhF10Yy/HeX9RkpbtXMJzOEw9Jqzp0aULdWzBK3kXE0hnPIAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB5qbma11B/82t9qXvZAFZ5haOJbMREAsqAAAAAAAAAAAAAAAAAAAAAAAA//9k=',
        info: {
            name: 'בלה1'
        }
    },
    {
        type: 'שולחנות',
        url:'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEA8QDw8QDxAQEA0PDw8PDw8PDQ8PFREWFhURFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OFQ8PFisdFR0rKy0tKy0rLSstKy0tKy0rNzcrKy0tLisrLS03NysrNy0tLisrKysrKysrNzcrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAAAgQBAwUGBwj/xABJEAACAQEDBggLBQYEBwAAAAAAAQIDBBEhBRIxUZHRB0FTVGFxkrEGExUWIjR0gaK08CQ1UnOhFDJyo7PBJUJj4SMzQ2KUpNL/xAAXAQEBAQEAAAAAAAAAAAAAAAAAAQID/8QAIBEBAAEEAwEBAQEAAAAAAAAAAAECETFRAxJBE4FhIf/aAAwDAQACEQMRAD8A9xAAAAAAAAANU6yXSwNpCVRLSytOq30dRACw7RqRrdeXUawBLxktbMZ71vazAAznvW9rM+MetkQBsVeXWTVo1rYaABcjVT49pMoE4VGgLgNUK6enDuNoAAAAAAAAAAAAAAAAAjKSWkxUqJbipOTekCdSs3owRrBxFv8ACClSm6d0qjj+84uN0X+HF6d5JmIysRM4cuDgfOinyVTbDePOinyNX4N5nvTtr51ac8DgPOinyNX4N5B+FlPkKv8AL3j6U7PnVp2IHXo+FdPkaq683ePOunyVTbDeO9Oz51adhB11+FlPkau2nvIPwwp8hW/l7y96dnzq07KDrPnlT5Cttp7zHnnT5vW+DeO9Oz51adnB1fz0pchW+DeYfhtS5vW/l7x3p2fOrTtJOnVa6VqOByN4S0rTN01GdKV18FUzfTXHm3PSsGc2WJicMzExlcpzT0EyjF3aC1Sq39ZUbAAAAAAAAAAAIVamau4zKVyvZTnK93sBKV+LMAAD588NrbKz2q1OMYzzrZaldLOV3pylxH0GfOnCV6zaPbbT3yMV+OnHNruH8458jS21N4fhHPkaW2e84UkZ6w13nbmF4Qz5GltnvDy/PkaW2e84mCwNuYTrCxVVtyCy/Pkqe2e82Ucu4/8AEo+jc/8AlTzZX8WMk1dp3nFxpkswWjS9p25Gtlx5z8XSSh/l8ZJyqXdLiktiNflmb/6dPbPeU1Ayoi0HadrPlmfJU9s95nyxPk4bZ7ylmk1AWgvK15YnycPi3kfLE+ThtnvK7gR8Xg/eLQXnbu3BzXlUyjk9uMY31a+i9/u2etr6j3w8B4M43ZRyd+Zafl6578bow58mYAmAbc1qjUv6zaUE7sUXKc71ftAmAAAAAAGuvO5dLA0153voRqAAAADB868JXrNo9ttPfI+iz514SvWbR7bae+Rivx0o9dMJkETCNlM3XGqloRvuwMy3CSQUcScUSisSNWRzQom5IKJLrZXlDE2RiJxxJxVwuRCEokFHB+8szj3Gq7B+8lyYdr4N/vHJ38do+WrHvZ4NwcfeWT/47R8tWPeTpx4c+XMAAOjkEqU7n0cZEAX0wabPPC7Vo6jcAAAAqVp3v9CxUlcmymAAAAAAD504SvWLR7bae+R9FnzpwmesWj22098jFfjpRiXS0yecaiSCLFF4FlPAqU5G/wAYta2oxLpCwmSgyuqi1raicai1raiWautpmE9JojVWtbUZVRY4raiWW7ZN47DKZrnNYYrajKksMVtQPVip/Y0cTJVaq1rR+JGtVFc8VtXQIWXb+Dn7yyf/AB2j5ase8ngvBz95ZP8AzLR8tWPejpx4lx5cwAA6OQAAJU5XNMuooFuhK9dWAGwAAaLS9CK5stD9LqNYAAAAAAPn3w9sUq1qtMYyhFq12mTc3mq7Oaw6cT6CPnXhLX2iv7bae+Rivx0o9cIvB2fHUo9p7jPm9LlKXae44pRWpGc1XaEZs1eNOV8gy5Sl2pbh5ClylLty3HFxgtX6E/FrURq8acj5CnylLty3EXkOfKUu1LcUVSWpGfFK7QC8aXfIc/x0u3ILI0+UpdtlGNJajPiljgC8aXnkifKUu2w8kz/HT7cig6P1cS8V0Cxf+LfkmX46faYeS5fjpdqRTnRWog6SueCFi8ad74N6bjlLJ8W02qloV6d6a/Zqx70eA8GEbsoZOX+raX/69Y9+N0YljkzAADbkAAAbrM8WjSTov0kBcBgAU6v7z6yJmel9bMAAAAAAA+d+Ev1iv7bae+R9EHzxwmesVvbLR3yMV+OnHiXTYkmYgCK20+I23aDXRNyRlqCKJqOBiOkmiKgoGEsTc43GILT7hdbNc44omoYJkqyxRszfREyRDROm9ppSwZfqrR1Lu4irdg/f3EiVmHbuDX7xyd+ZaPlqx72eCcHP3lk/8y0fL1j3s6UYcuXMAAOjkAAAZjp2GABevBEAVJ6X1swSqrF9ZEAAAAAAHzrwles2j2y0d8j6KPnThL9ZtHttp75GK/HSj10+myV+P10GhSMqQIWaMsX1stZ3eUKcjepmJaiVqLxZO8rRkbM4jULU3iyEHp6jXKYhIlmrpN+kb1P0Nt20qSeJtvwEkLNV4LqXcV3ofW+42VHhHqXca1+6/riJDUu18HX3lk/8y0fLVj3o8G4OfvKwfmWj+hVPeTrRhw5cgAOjkAAAAFuAuAlcAKtoXpdZrN9qWh+40AAAAAAA+deEz1m0e2WnvkfRR8/eHleELXaZVKSrJ2u0pRk7knnN52jHQ0c6/HTj9dCzOgKi9X6nNrKlHmdPatxjynRw+yU9q3Gby1aNuIVB/UjPinrfaOXWU6PHZKe3/YksoUeZ0+0twvOmrRtw6g9b7RnHp7Zy/lCjzOG1f/JBZQpc0p7VuJedFo24xX632xjrfbOXVupc0pfpuDttLmlL9Nw/C0bcQ29b7ZnOet9s5P8AbaWP2Wn+m4z+10+bU/h3A/XHeMlrfbCk9bx/70clK1U7vVofDuIftlPm0MNVy/sF/XZ+DSV+UMnN/jtF/wD49Y99PBODaopZRsEowUE6to9FaPV6p72dKMOfLmAAG3IAAAlSXpIibbMsb9QFm8GQBCrG9Mpl8p1Y3MCAAAAAAfO3CT6xX9ttPfI+iT524SfWa/ttp75GK/HSjEuoIxLi+uIRI1Ho9wG2mbru9Gig8CzeZlqBR0mXDEnDQSWl9TI1ZCEdP1xk3HBCP1tJx4vriJdbK7WOw2wp3p4EHpfuLFOfovqb/QTKRCDhgtJoUdPvLlXQv7dRXeiXX/ZiJWYdt4MY/b8nfmWn5eqe+M8E4NH9vyf+baPl6p7u5G+PEufL4neYvNbkYzjo5Nt4vNOcZzgNt5bs0fRv14lGCzmlrOTSAyAABqtEL1fq7jaAKAJ1oXPo4iAAAAD534SX9or+22nvkfRB86cJL+02j26098jFfjpRiXUYMhJ6PcQzheBYs7N7kUqcjc5GWoWYTNqk731FOnI3KZGm6D0/WonF4r64jRBklLQRWG8X9cZJzwwd3Qam8TL3lRctDwXQsbrtJWvwfvJVJGpPB+8kLLuHBu/8QsHRUr/0Kp7pnHhPB0/8QsP5tf8AoVT3HOOlGHPlzDZnGM413i825Nl4vIG2hScpJL3vUgLmT6f+Z8eCLpiMUkkuIyAAAAAARnC9XFNq7Bl411qd/WBUAaAA+c+Ep/aLR7bae+R9Fs+evD+yzq2q0xpxvatlpb9KMblnSXGYr8dOP10Roxicv5Dral2obx5Gq9G2O8z2hrpLiVf9InnS6Nhyfkmr0bY7w8l1vpx3i69JcdGcujYycZy6C68mVvpx3mY5MrfTjvJdesqiqS6B459GwurJlbWtsd4qZLrcePvjvF4XrKj4x9Gwz419GwtxyZV1fFAz5Mq9HaiLwlpVXXl0bGYVWWPv4mW/Jlbo7USLydV6O1En+Fpdr4Onfb7A/wDUrP8Ak1D2+88Q4O6Uo2+xKWlVKvGng6VTUe3I6UOfJmGSSRmMDbGmbc0IxvwOVstDMXS9L/sQstnzcXp7i0AAAAAAAAAAAGqrSv6yq1dpL5CpTT0gUjplDwGi7XabRaJU61OtKtKFLNms1zqKSlJ34tK9Yazu9Sm11ayBJi6xMxh17zNsHNofHvHmZYObU/j3nYTAtBeduveZdg5tT+PeH4F5P5rT+PedhAtB2l17zLsHNafx7x5l2DmtP4952EC0HaduvrwMsHNqfx7zHmZYObU/j3nYQOsaO07dffgbYObU/j3mPMzJ/Nafx7zsQFo0dp2675l5P5rT+PeYXgTk7mtP4952MC0HaXSKfgEqWUaNrs86dKhTubs+ZNyzvFzg3F33Jekn7nrO4Qs6RvJQg3o/2ERYmZnLXGBao0LsXp7idOkl16zYVAAAAAAAAAAAAAAAABmmdBcWHcbgBSlBrSiJfuNcqKfR1AVAbnZ3xMg6UtQEAZcevYYAAC76uAAkqctTNis742kBpMxi3oRZjQXWbEgNMKGvYbkjIAAAAAAAAAAAAAAAAAAAAAAAAAGAADIsAAiSAAyAAAAAAAAAAAAAAAAAAP/Z',
        info: {
            name: 'בלה1'
        }
    },
    {
        type: 'ארונות',
        url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQRCEdHRLuQIMvDGNq8Z8aWSDPhqnonfRo_liyIvfLryzDEAhhS1w',
        info: {
            name: 'בלה1'
        }
    },
    {
        type: 'כיסאות',
        url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTO_f6dTONo_HAJOpA-X564gYGQBb6CEW0tgbwMPc0NnzyRpV-Z',
        info: {
            name: 'בלה1'
        }
    },
    {
        type: 'סלונים',
        url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSdl508GcU7d3jXAC-iy9DpbvAnLa7btXXAEW7Ugvwc68p9sLOFVA',
        info: {
            name: 'בלה1'
        }
    },
    {
        type: 'סלונים',
        url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTuG0naC-qS3-2QFy6cLcn_4MrBz7jKgUC54Uqdwr9hBbpS-Q3lg',
        info: {
            name: 'בלה1'
        }
    },
    {
        type: 'סלונים',
        url: 'https://cdn-images-1.medium.com/max/2400/1*wGvMAPrzFujqFXxIz0va1A.png',
        info: {
            name: 'בלה1'
        }
    }
]
export default class GalleryContainer extends Component {
    
    static proptypes = {
        selectedImageUrl: PropTypes.string
    }

    constructor(props)  {
        super();
        this.state = {
            selectedImageIndex: 0, 
            options: [],
            selectedCategory: imageUrls && imageUrls.length > 0 && imageUrls[0].type
        }
        this.nextImage = this.nextImage.bind(this);
        this.prevImage = this.prevImage.bind(this);
    }

    componentDidMount = () => {
        this.setState({
            options: imageUrls.filter(i => i.type == this.state.selectedCategory),
            selectedImageIndex: 0
        });
    }

    handleCategoryClick = (name) => {
        this.setState({ 
            selectedCategory: name,
            options: imageUrls.filter(i => i.type == name),
            selectedImageIndex: 0
        });
    }

    nextImage = () => {
        if (this.state.selectedImageIndex + 1 < this.state.options.length ) {
            this.setState({selectedImageIndex: this.state.selectedImageIndex + 1});
        }
    }
    prevImage = () => {
        if (this.state.selectedImageIndex > 0) {
            this.setState({ selectedImageIndex: this.state.selectedImageIndex - 1 });
        }
    }
    render() {
        const imageObject = this.state.options &&
            this.state.options[this.state.selectedImageIndex] || null;
        
        const imageTitle = imageObject && imageObject.info && imageObject.info.name;
        const imageURL = imageObject && imageObject.url;

        return (
            <Gallery
                nextImage={this.state.selectedImageIndex + 1 < this.state.options.length && this.nextImage}
                prevImage={this.state.selectedImageIndex > 0 && this.prevImage}
                handleCategoryClick={this.handleCategoryClick}
                selectedImageTitle={imageTitle}
                selectedImageSrc={imageURL}/>
        )
    }
}
