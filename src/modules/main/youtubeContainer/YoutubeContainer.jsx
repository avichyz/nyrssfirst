import React, { Component } from 'react';
import YouTube from 'react-youtube';

class YoutubeContainer extends Component {
      render() {
    const opts = {
      height: '120px',
      width: '100%',
      playerVars: { 
        // https://developers.google.com/youtube/player_parameters
        autoplay: 1
      }
    };

    return (
      <YouTube
        videoId="6dHrafwh974"
        opts={opts}
        onReady={this._onReady}
      />
    );
  }

  _onReady(event) {
    // access to player in all event handlers via event.target
    // event.target.playVideo();
  }
}

export default YoutubeContainer;
