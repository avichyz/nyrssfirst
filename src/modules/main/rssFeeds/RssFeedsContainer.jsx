import React, { Component } from 'react';
import styles from './rssFeedsContainer.scss';
import AutoScroll from '../../../components/autoScroll/AutoScroll';
import classNames from 'classnames';

class RssFeedsContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            shouldRender: false
        }

        this.loadFeeds = this.loadFeeds.bind(this);
    }
    componentDidMount() {
        this.loadFeeds();
    }

    loadFeeds() {
        const envVars = this.props.envVars;
        const URLS = envVars.URLS;
        const CORS_PROXY = envVars.CORS_PROXY;
        const API_KEY = envVars.API_KEY;

        (async () => {
            let items = [];

            for (let index = 0; index < URLS.length; index++) {
                URL = URLS[index];
                let feed = await fetch(`${CORS_PROXY}?rss_url=${URL}&api_key=${API_KEY}&count=15`);
                feed = feed.body.getReader();
                feed = await feed.read();
                try {
                    let newItems = JSON.parse(new TextDecoder("utf-8").decode(feed.value)).items.filter(x => (!x.content.includes("סקס")) && (!x.content.includes("סרטן")) && !x.title.includes("סרטן"));
                    items = items.concat(newItems);
                }
                catch (e) {
                    console.log("Error in rss" + e + "in link:" + URL);
                }
            }
            this.setState({ items: items })
        })()
    }

    render() {
        const { className, envVars } = this.props;

        return (
            this.state.items && this.state.items.length > 0 &&
            <AutoScroll
                id="rssFeeds"
                className={classNames(styles.rssScroll, className)}
                speedDuplicator={envVars.rssSpeed || 0.3}
                callbackOnCompletionOfScroll={this.loadFeeds}>
                {
                    this.state.items.map((item, index) => {
                        const content = item.content || item.description;
                        return <div key={"item-" + index} className={styles.item}>
                            <div className={styles.title}>{item.title}</div>
                            <div className={styles.content} dangerouslySetInnerHTML={{ __html: content }} />
                            <hr></hr>
                        </div>
                    })
                }
                <div className={styles.endDelimitet}>End Of rss data</div>
            </AutoScroll> ||
            <div>טוען נתונים</div>
        )
    }
}

export default RssFeedsContainer;
