import React, { Component } from 'react';
import UpdatesContainer from './updatesContainer/UpdatesContainer';
import RssFeedsContainer from './rssFeeds/RssFeedsContainer';
import styles from './main.scss';
import YoutubeContainer from './youtubeContainer/YoutubeContainer';
import SaturdayFooterContainer from './saturdayFooter/SaturdayFooterContainer';
import WeatherContainer from './weatherContainer/WeatherContainer';
import NightPlaceHolder from './nightPlaceHolder/NightPlaceHolder';


class MainContainer extends Component {
    constructor(props) {
        super(props);
        this.manageSilencing = this.manageSilencing.bind(this);
        this.shouldSilenceMusic = this.shouldSilenceMusic.bind(this);
        this.state = {
            showPlayer: true,
            envVars: {
                updatesSpeed: 0.1,
                rssSpeed: 0.3,
                API_KEY: 'cgc5kdnnssejywpwi6iftb7bnemd0yl1lssylofl',
                CORS_PROXY: "https://api.rss2json.com/v1/api.json",
                URLS: [
                    'http://www.ynet.co.il/Integration/StoryRss1854.xml',
                    'http://www.ynet.co.il/Integration/StoryRss2.xml',
                    'http://rcs.mako.co.il/rss/news-military.xml',
                    'http://rcs.mako.co.il/rss/news-israel.xml',
                    'https://www.globes.co.il/webservice/rss/rssfeeder.asmx/FeederNode?iID=2',
                    'http://rcs.mako.co.il/rss/MainSliderRss.xml'
                ]
            }
        }
    }
    componentDidMount() {
        this.manageSilencing();
        setInterval(() => { 
            this.manageSilencing();
        }, 
        60000);
    }

    manageSilencing() {
            if(this.state.showPlayer && this.shouldSilenceMusic()) {
                this.setState({showPlayer: false})
            }
            else if (!this.state.showPlayer && !this.shouldSilenceMusic()) {
                this.setState({showPlayer: true})
            }
    }
    
    shouldSilenceMusic() {
        const currentDateTime  = new Date();
        const hour = currentDateTime.getHours();  // 0-23
        const dayOfWeek = currentDateTime.getDay(); // 0 - 6
        const {isSaturday, setSaturday, setHeaderTitle} = this.props;
        // check if saturday
        if((dayOfWeek == 5 && hour >= 11) || (dayOfWeek == 6 && hour < 20)) {
            if(!isSaturday) {
                setHeaderTitle("שבת שלום");
                setSaturday(true);
            }
            return true;
        }
        else if (isSaturday || hour == 0) {
            setSaturday(false);
        }

        // if after 23:00
        if(hour >= 22 || hour < 6) {
            setHeaderTitle("לילה טוב");
            return true;
        }
        else if(hour >=6 && hour <= 13) {
            setHeaderTitle("בוקר טוב");
        }
        else if(hour >=14 && hour <= 17) {
            setHeaderTitle("צהריים טובים");
        }
        else if(hour >=18 && hour < 22) {
            setHeaderTitle("ערב טוב");
        }
        return false;
    }
    render() {
        return (
            <div className={styles.mainContainer}>
                <div className={styles.leftSide}>
                    <UpdatesContainer
                        className={styles.updatesContainer}
                        envVars={this.state.envVars} />
                    {   
                        !this.props.isSaturday && this.state.showPlayer &&    
                        <YoutubeContainer/> || 
                        <NightPlaceHolder/>
                    }
                    {
                        this.props.isSaturday && 
                        <SaturdayFooterContainer /> 
                    }
                    {
                        <WeatherContainer />
                    }
                </div>
                <div className={styles.rightSide}>
                <RssFeedsContainer
                    className={styles.rssFeedsContainer}
                    envVars={this.state.envVars} />
                </div>
            </div>
        )
    }
}

export default MainContainer;
