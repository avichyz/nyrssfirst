import React, { Component } from 'react';
import AutoScroll from '../../../components/autoScroll/AutoScroll';
import classNames from 'classnames';
import styles from './updatesContainer.scss';

const items = [
    {
        title: "שבוע טוב!",
        content: ""
    },
    {
        title: "נא לשמור על ניקיון הבניין",
        content: "ניקיון הבניין הוא לרווחתכם האישית"
    },
    {
        title: "מחפשים ראש וועד חדש שייתמודד מול חברת האחזקה",
        content: "פנה לוועד אם אתה מאמין שיש בך את זה"
    }
]
class UpdatesContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: items
        };
    }
    componentDidMount() {
        // load messages (updates) from server 
    }
    render() {
        const { className, envVars } = this.props;

        return (
            this.state.items && this.state.items.length > 0 &&
            <AutoScroll
                id="updates"
                speedDuplicator={envVars.updatesSpeed || 0.1}
                className={classNames(styles.updatesContainerScroll, className)}>
                {
                    this.state.items.map((item, index) => {
                        const content = item.content || item.description;
                        return <div key={"item-" + index} className={styles.item}>
                            <div className={styles.title}>{item.title}</div>
                            <div className={styles.content} dangerouslySetInnerHTML={{ __html: content }} />
                        </div>
                    })
                }
            </AutoScroll> ||
            <div>טוען נתונים</div>
        );
    }
}

export default UpdatesContainer;
